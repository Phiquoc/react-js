import React from "react";
import ChildComponent from "./ChildComponent";

class okComponent extends React.Component {
    state = {
        first_name:'',
        last_name :'',
        arrJobs:[      
            { id:'acd1', title: 'Quang Đạt', salary: '800$'},
            { id:'acd2', title: 'Quang An',salary: '8000$' }
        ]
      

    }
    changeFirstname = (event) =>{
        this.setState({

            first_name: event.target.value
        }
        )  
    }
    changeLastname = (event) =>{
        this.setState({

            last_name: event.target.value
        }
        )  
    }
    handleSubmit = (event) =>{
        event.preventDefault();
        console.log(this.state);
    }
    render() {
   
        return (
           
            <>
             
                <form>
                    <label htmlFor="fname">First name:</label>
                    <input type="text" id="fname" name="fname" value={this.state.first_name} onChange={(event) => this.changeFirstname(event)} /><br /><br />
                    <label htmlFor="lname">Last name:</label>
                    <input type="text" id="lname" name="lname" value={this.state.last_name} onChange={(event) => this.changeLastname(event)} /><br /><br />
                    <input type="submit" value="Submit" onClick={(event)=> this.handleSubmit(event)} />
                </form>
                <ChildComponent 
                name={this.state.first_name} 
                age = {'25'}
                abc = {this.state.arrJobs}/>
               
            </>
        )


    }
}

export default okComponent;

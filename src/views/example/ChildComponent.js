import React from "react";


// class ChildComponent extends React.Component {
//     render() {

//         let { name, age, abc} = this.props;
//         return (

//             <>
//                 <div>{name} - {age}  </div>
//                 <div className="list-job">
//                     {abc.map((item, index)=>{
//                         return(
//                             <div key={item.id}>
//                                 salary {item.salary}
//                             </div>
//                         )
//                     })}
//                 </div>
//             </>
//         )


//     }
// }
const ChildComponent =(props)=>{
    console.log("<<< Check props ", props )
    let { name, age, abc} = props;
        return (

            <>
                <div>{name} - {age}  </div>
                <div className="list-job">
                    {abc.map((item, index)=>{
                        return(
                            <div key={item.id}>
                                salary {item.salary}
                            </div>
                        )
                    })}
                </div>
            </>
        )
}
export default ChildComponent;
